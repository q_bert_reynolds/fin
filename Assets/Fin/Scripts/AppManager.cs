﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FishGame {
public class AppManager : MonoBehaviour {

    public static AppManager instance;

    public Color[] playerColors = new Color[] {
        Color.white,
        Color.red,
        Color.green,
        Color.blue,
        Color.yellow,
        Color.cyan,
        Color.magenta,
        Color.gray,
        Color.black   
    };

    [Range(0,4)] public int playerCount = 1;
    [Range(2, 20)] public int racerCount = 8;

    void Awake () {
        if (instance == null) instance = this;
    }
}
}
