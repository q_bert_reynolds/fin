﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Paraphernalia.Extensions;
using Paraphernalia.Utils;

public class Track : MonoBehaviour {

	public int laps = 3;
	public LayerMask envMask;
	public Vector2 startPoint;

	[Range(1, 20)]public int subdivisions = 10;
	public Vector2[] wayPoints = new Vector2[] {
		Vector2.left, Vector2.up, Vector2.right	
	};

	[SerializeField, HideInInspector] private GameObject[] _checkpoints;
	public GameObject[] checkpoints {
		get { return _checkpoints; }
	}

	public Vector2 GetCatmullRomPoint (int wayPoint, float t) {
		if (t < 0) {
			wayPoint--;
			t++;
		}

		int len = wayPoints.Length;
		Vector2 p0 = wayPoints[(wayPoint+len-1)%len];
		Vector2 p1 = wayPoints[(wayPoint+len)%len];
		Vector2 p2 = wayPoints[(wayPoint+1)%len];
		Vector2 p3 = wayPoints[(wayPoint+2)%len];
		return Interpolate.CatmullRom(p0, p1, p2, p3, t);
	}

	public void Setup () {
		List<GameObject> checkpointList = new List<GameObject>(GameObject.FindGameObjectsWithTag("Checkpoint"));
		int startCount = checkpointList.Count;
		int cp = 0;
		int len = wayPoints.Length;
		for (int i = 0; i < len; i++) {
			for (float j = 0; j < subdivisions; j++) {
				float t0 = (j-1) / (float)subdivisions;
				float t1 = j / (float)subdivisions;
				float t2 = (j+1) / (float)subdivisions;
				Vector2 c0 = GetCatmullRomPoint(i, t0);
				Vector2 c1 = GetCatmullRomPoint(i, t1);
				Vector2 c2 = GetCatmullRomPoint(i, t2);

				GameObject checkpoint = null;
				if (cp < startCount) {
					checkpoint = checkpointList[cp];
				}
				else {
					checkpoint = new GameObject();
					checkpointList.Add(checkpoint);
				}
				
				checkpoint.name = "Checkpoint" + cp;
				checkpoint.tag = "Checkpoint";
				
				Transform t = checkpoint.transform;
				t.SetParent(transform);
				t.localScale = Vector3.one;
				t.localPosition = c1;
				t.right = c2 - c0;

				RaycastHit2D hitA = Physics2D.Linecast(t.position, t.position + t.up * 30, envMask);
				RaycastHit2D hitB = Physics2D.Linecast(t.position, t.position - t.up * 30, envMask);
				Vector2 a = (hitA.collider != null) ? hitA.point : (Vector2)(t.position + t.up * 30);
				Vector2 b = (hitB.collider != null) ? hitB.point : (Vector2)(t.position - t.up * 30);
				BoxCollider2D box = checkpoint.GetOrAddComponent<BoxCollider2D>();
				box.isTrigger = true;
				box.size = new Vector2(1, Vector2.Distance(a, b));
				t.position = (a + b) * 0.5f;

				cp++;
			}
		}
		if (startCount > cp) {
			for (int i = startCount-1; i >= cp; i--) {
				GameObjectUtils.Destroy(checkpointList[i]);
				checkpointList.RemoveAt(i);
			}
		}
		
		_checkpoints = checkpointList.ToArray();
	}

	void OnDrawGizmos () {
		Gizmos.DrawWireSphere(startPoint, 1);
	}
}
