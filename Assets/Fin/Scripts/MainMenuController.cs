﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using Paraphernalia.Components;

namespace FishGame {
public class MainMenuController : MonoBehaviour {

    public Transform selector;
    [Range(1,4)] public int maxPlayers = 2;

    bool pressed = false;
    float startTime;
    float waitTime = 1;
    float[] lastX;

    void Start () {
        Cursor.visible = false;
        startTime = Time.time;
        lastX = new float[maxPlayers];
    }

    public void StartPressed (int players) {
        if (pressed) return;

        maxPlayers = players;
        AppManager.instance.playerCount = players;
        pressed = true;
        AudioManager.PlayEffect("confirm");
        SceneManager.LoadScene("Game", LoadSceneMode.Single);
    }

    public void QuitGame () {
        AudioManager.PlayEffect("confirm");
        Application.Quit();
    }

    GameObject lastSelection;
    public void SelectionChanged () {
        GameObject g = EventSystem.current.currentSelectedGameObject;
        selector.transform.position = g.transform.position;
        if (g != lastSelection && lastSelection != null) {
            AudioManager.PlayEffect("select");
        }
        lastSelection = g;
    }

}
}
