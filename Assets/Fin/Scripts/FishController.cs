﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Paraphernalia.Extensions;

namespace FishGame {
public class FishController : MonoBehaviour {

	public delegate void FishEvent(FishController fish);
	public static event FishEvent onCheckpointReached = delegate {};
	public static event FishEvent onNewLap = delegate {};
	public static event FishEvent onFinish = delegate {};

	public bool isDemo = false;
	public int player;

	public float minSpeed = 1;
	public float maxSpeed = 10;
	public float turnSpeed = 10;
	public float maxSpeedChange = 1;
	public float dashIncrease = 0.5f;
	public float speedDamping = 0.1f;

	private int _lap = 0;
	public int lap {
		get { return _lap; }
	}

	private int _checkpoint = -1;
	public int checkpoint {
		get { return _checkpoint; }
	}

	private float _timeAtCheckpoint;
	public float timeAtCheckpoint {
		get { return _timeAtCheckpoint; }
	}

	float speed = 1;

	bool _inWater = true;
	public bool inWater {
        get { return _inWater; }
    }

	float targetAngle;
    Vector2 _targetHeading;
    public Vector2 targetHeading {
        get { return _targetHeading; }
        set {
            _targetHeading = value;
            if (_targetHeading.magnitude > 0.1f) {
				targetAngle = Mathf.Atan2(_targetHeading.y, _targetHeading.x) * Mathf.Rad2Deg;
			}
        }
    }

	Rigidbody2D body;
	Animator anim;
	SpriteRenderer sprite;
	ParticleSystem bubbles;

    void Start () {
		body = GetComponent<Rigidbody2D>();
		anim = GetComponent<Animator>();
		sprite = GetComponent<SpriteRenderer>();
		bubbles = GetComponentInChildren<ParticleSystem>();
	}

	void Update () {
		if (!isDemo && !GameManager.canRace) return;

		if (inWater) {
			speed -= speedDamping * Time.deltaTime;
			speed = Mathf.Clamp(speed, minSpeed, maxSpeed);
			transform.rotation = Quaternion.Slerp(
				transform.rotation, 
				Quaternion.AngleAxis(targetAngle, Vector3.forward),
				turnSpeed * Time.deltaTime
			);
		}
		else if (body.velocity.magnitude > 0.1f) {
            float angle = Mathf.Atan2(body.velocity.y, body.velocity.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
		}

		sprite.flipY = Vector3.Dot(Vector3.right, transform.right) < 0;
        anim.SetFloat("speed", (speed - minSpeed) / (maxSpeed - minSpeed));
		ParticleSystem.EmissionModule e = bubbles.emission;
		e.rateOverTime = speed;
	}

	void FixedUpdate () {
		if (!isDemo && !GameManager.canRace) return;

		if (inWater) {
			Vector2 change = ((Vector2)transform.right * speed - body.velocity);
			change = change.normalized * Mathf.Min(change.magnitude, maxSpeedChange);
			body.AddForce(change, ForceMode.VelocityChange);
		}
		else {
			body.AddForce(transform.right);
		}
	}

	void OnTriggerEnter2D (Collider2D c) {
		if (c.gameObject.layer == 4) {
			body.gravityScale = 0;
			_inWater = true;
			bubbles.Play();
		}
		else if (c.gameObject.CompareTag("Checkpoint")) {
			int prog = GameManager.GetProgress(c.gameObject.GetInstanceID());	
			if (prog == (_checkpoint + 1) % GameManager.checkpointCount) {
				_timeAtCheckpoint = GameManager.elapsedTime;
				if (lap == GameManager.laps && prog == 0) onFinish(this);
				if (prog == 0) {
					_lap++;
					onNewLap(this);
				}
				onCheckpointReached(this);
				_checkpoint++;
				_checkpoint = _checkpoint % GameManager.checkpointCount;
			}
		}
	}

	void OnTriggerExit2D (Collider2D c) {
		if (c.gameObject.layer == 4) {
			body.gravityScale = 1;
			_inWater = false;
			bubbles.Stop();
		}
	}

	void OnCollisionExit2D (Collision2D c) {
		speed = body.velocity.magnitude;
	}

    public void Dash () {
        speed += dashIncrease;
    }
}
}