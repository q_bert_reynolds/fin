﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace FishGame {
public class PlayerUIController : MonoBehaviour {
    
    public int player;
    public Text lapText;
    public Text positionText;
    public Text finishText;

    void OnEnable () {
        FishController.onNewLap += UpdateLap;
        GameManager.onResult += FinishRace;
    }

    void OnDisable () {
        FishController.onNewLap -= UpdateLap;
        GameManager.onResult -= FinishRace;
    }

    void Start () {
        UpdateLap(1);
    }

    public void UpdateLap (FishController fish) {
        if (fish.player == player) UpdateLap(fish.lap);
    }

    public void FinishRace (FishController fish, int rank) {
        if (fish.player != player) return;

        finishText.gameObject.SetActive(true);
        string[] ranks = new string[] {
            "1st Place", 
            "2nd Place", 
            "3rd Place", 
            "4th Place", 
            "5th Place", 
            "6th Place", 
            "7th Place", 
            "Dead Last"
        };

        finishText.text = ranks[rank];
    }

    public void UpdateLap (int lap) {
        lapText.text = "Lap " + Mathf.Min(lap, GameManager.laps).ToString() + " / " + GameManager.laps.ToString();
    }

    void Update () {
        int i = GameManager.GetRanking(player) + 1;
        positionText.text = i.ToString() + " / " + AppManager.instance.racerCount.ToString();
    }
}
}