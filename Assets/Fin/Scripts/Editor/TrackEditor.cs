﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Paraphernalia.Extensions;

[CustomEditor(typeof(Track))]
public class TrackEditor : Editor {

	const float handleScale = 0.2f;

	Track track {
		get { return target as Track; }
	}

	public override void OnInspectorGUI() {
		base.OnInspectorGUI();
		if (GUILayout.Button("Rotate")) track.wayPoints = track.wayPoints.Shift(-1);
		if (GUILayout.Button("Reverse")) track.wayPoints = track.wayPoints.Reverse();

	}
	
	void OnSceneGUI() {
		if (target == null) return;

		Event e = Event.current;

		if (track.wayPoints.Length == 0) return;

		Handles.matrix = track.transform.localToWorldMatrix;
		Handles.color = Color.blue;

		Vector2[] points = track.wayPoints;
		int len = points.Length;
		for (int i = 0; i < len; i++) {
			for (float j = 0; j < track.subdivisions; j++) {
				float t0 = j / (float)track.subdivisions;
				float t1 = (j+1f) / (float)track.subdivisions;
				Vector2 c0 = track.GetCatmullRomPoint(i, t0);
				Vector2 c1 = track.GetCatmullRomPoint(i, t1);
				Handles.DrawLine(c0, c1);	
			}
		}

		EditorGUI.BeginChangeCheck();

		if (e.alt) RemovePoint();
		else {
			if (e.shift) AddPoint();
			for (int i = 0; i < len; i++) {
				Vector2 point = points[i];
				
				Handles.color = Color.white;
				GUI.SetNextControlName("track point " + i);
				float size = GetHandleSize(point, 1);
				point = Handles.FreeMoveHandle(
					point, 
					Quaternion.identity, 
					size, 
					Vector3.zero, 
					Handles.CircleHandleCap
				);

				Handles.Label(point + new Vector2(-size * 0.25f, size * 0.5f), i.ToString());

				points[i] = point;
			}
		}

		if (EditorGUI.EndChangeCheck()) {
			Undo.RecordObject(target, "moved track point");
			track.wayPoints = points;
			track.Setup();			
			EditorUtility.SetDirty(target);
		}
	}

	void AddPoint () {
		List<Vector2> points = new List<Vector2>(track.wayPoints);
		int len = track.wayPoints.Length;
		for (int i = 0; i < len; i++) {
			int n = (i+1)%len;
			Vector2 p1 = track.wayPoints[i];
			Vector2 p2 = track.wayPoints[n];
			Handles.color = Color.green;
			GUI.SetNextControlName("remove track point " + i);
			Vector2 mid = (p1 + p2) * 0.5f;
			float size = GetHandleSize(mid, 0.5f);
			if (Handles.Button(mid, Quaternion.identity, size, size, Handles.CircleHandleCap)) {
				points.Insert(n, mid);
				Undo.RecordObject(target, "added track point");
				track.wayPoints = points.ToArray();
				track.Setup();			
				EditorUtility.SetDirty(target);
				break;
			}
		}
	}	

	void RemovePoint (int index) {
        if (index < 0 || index >= track.wayPoints.Length) return;

        Undo.RecordObject(target, "removed track point");
		List<Vector2> points = new List<Vector2>(track.wayPoints);
        points.RemoveAt(index);
        track.wayPoints = points.ToArray();
		track.Setup();
		EditorUtility.SetDirty(target);
    }

    void RemovePoint () {
    	for (int i = 0; i < track.wayPoints.Length; i++) {
			Handles.color = Color.red;
   		 	float size = GetHandleSize(track.wayPoints[i], 1);
   			GUI.SetNextControlName("remove pretty poly point " + i);
			if (Handles.Button(track.wayPoints[i], Quaternion.identity, size, size, Handles.CircleHandleCap)) {
				RemovePoint(i);
				break;
			}
		}
    }

    float GetHandleSize (Vector3 pos, float size) {
    	return HandleUtility.GetHandleSize(pos) * size * handleScale;
	}
}
