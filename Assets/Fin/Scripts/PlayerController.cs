﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Paraphernalia.Components;

namespace FishGame {
public class PlayerController : MonoBehaviour {

    public int player;

    FishController fish;

    void Start () {
        fish = GetComponent<FishController>();
    }

    void Update () {
        if (fish.isDemo || (fish.inWater && GameManager.canRace)) {
            fish.targetHeading = new Vector2(
                Input.GetAxis("Horizontal" + player),
                Input.GetAxis("Vertical" + player)
            );

            if (Input.GetButton("Dash" + player)) fish.Dash();
        }
    }

    void OnTriggerExit2D (Collider2D c) {
        if (c.gameObject.layer == 4) AudioManager.PlayVariedEffect("splash");
    }

    void OnTriggerEnter2D (Collider2D c) {
        if (c.gameObject.layer == 4) AudioManager.PlayVariedEffect("splash");
    }

    void OnCollisionEnter2D (Collision2D c) {
        AudioManager.PlayVariedEffect("boing");
    }
}
}