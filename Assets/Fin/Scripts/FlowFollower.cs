﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Paraphernalia.Components;

namespace FishGame {
public class FlowFollower : MonoBehaviour {

    public LayerMask flowLayer;
    [Range(0,1)] public float noise = 0.01f;

    FishController fish;

    static Dictionary<Coord, Vector2> headingDict = new Dictionary<Coord, Vector2>();
    public struct Coord {
        public int x;
        public int y;

        public Coord (int x, int y) {
            this.x = x;
            this.y = y;
        }
    }
    
    void Start () {
        fish = GetComponent<FishController>();

    }
    
    void Update () {
        if (fish.isDemo || (fish.inWater && GameManager.canRace)) {
            int pX = (int)(transform.position.x * 3);
            int pY = (int)(transform.position.y * 3);
            Coord coord = new Coord(pX, pY);
            if (headingDict.ContainsKey(coord)) {
                fish.targetHeading = headingDict[coord];
                fish.Dash();
            }
            else {
                RaycastHit hit;
                if (Physics.Raycast(transform.position, Vector3.forward, out hit)) {
                    Vector2 uv = hit.textureCoord;
                    Renderer r = hit.collider.gameObject.GetComponent<Renderer>();
                    Texture2D tex = r.material.mainTexture as Texture2D;
                    Color c = tex.GetPixelBilinear(uv.x, uv.y);
                    float x = 2 * c.r - 1 + Random.Range(-noise, noise);
                    float y = 2 * c.g - 1 + Random.Range(-noise, noise);
                    fish.targetHeading = new Vector2(-x, -y).normalized;
                    fish.Dash();
                    headingDict[coord] = fish.targetHeading;
                }
            }
        }
    }
}
}