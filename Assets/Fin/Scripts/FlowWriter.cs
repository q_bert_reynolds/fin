﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FishGame {
public class FlowWriter : MonoBehaviour {

	public float writeInterval = 1;
	public LayerMask flowLayer;
	public Material flowWriteMaterial;
	[Range(0, 1)] public float blend = 0.1f;
	
	public RenderTexture renderTexture;
	public Texture2D flowTexture;
	public FishController fish;
	public float radius;

	void Start () {
		fish = GetComponent<FishController>();

		RaycastHit hit;
		if (Physics.Raycast(transform.position, Vector3.forward, out hit)) {
			Vector2 uv = hit.textureCoord;
			Renderer r = hit.collider.gameObject.GetComponent<Renderer>();
			radius = 2 * GetComponent<Renderer>().bounds.extents.y / r.bounds.extents.y;
			flowTexture = r.material.mainTexture as Texture2D;
			renderTexture = new RenderTexture(flowTexture.width, flowTexture.height, 0, RenderTextureFormat.ARGB32);
			renderTexture.Create();
			Graphics.Blit(flowTexture, renderTexture);
		}
		else {
			Debug.LogError("No flowmap found");
		}
	}

	void LateUpdate () {
		if (fish.inWater && GameManager.canRace) {
			RaycastHit hit;
			if (Physics.Raycast(transform.position, Vector3.forward, out hit)) {
				Vector2 uv = hit.textureCoord;
				flowWriteMaterial.SetVector("_Transform", new Vector4(uv.x, uv.y, 0, radius));

				float r = transform.right.x * 0.5f + 0.5f;
				float g = transform.right.y * 0.5f + 0.5f;
				Color c = new Color(1-r, 1-g, 0, blend);
				flowWriteMaterial.SetColor("_Color", c);

				Graphics.Blit(renderTexture, renderTexture, flowWriteMaterial);
			}
		}
	}

	IEnumerator UpdateTexture () {
		yield return new WaitUntil(() => GameManager.canRace);
		yield return new WaitForSeconds(Random.value * writeInterval);
		while (enabled) {
			yield return new WaitForSeconds(writeInterval);
			RenderTexture.active = renderTexture;
			flowTexture.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0);
			flowTexture.Apply();
			RenderTexture.active = null;
		}
	}
}
}