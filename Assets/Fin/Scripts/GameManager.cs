﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Paraphernalia.Components;
using UnityEngine.SceneManagement;

namespace FishGame {
public class GameManager : MonoBehaviour {

    public static GameManager instance;
    public static int checkpointCount;
    public static bool canRace;

    public delegate void OnResult (FishController fish, int rank);
    public static event OnResult onResult = delegate {};

    public CameraController playerCameraPrefab;
    public FishController playerPrefab;
    public FishController npcPrefab;

    public Text bigText;

    public static int laps {
        get { return instance.track.laps; }
    }

    static float startTime;
    public static float elapsedTime {
        get { return Time.time - startTime; }
    }
    
    Track track;
    List<FishController> ranking = new List<FishController>();
    List<FishController> results = new List<FishController>();

    void Awake () {
        if (instance == null) instance = this; 
    }

    void Start () {
        track = FindObjectOfType<Track>();

        for (int i = 0; i < AppManager.instance.racerCount; i++) {
            FishController fish = null;
            if (i < AppManager.instance.playerCount) {
                fish = Instantiate(playerPrefab) as FishController;

                PlayerController player = fish.GetComponent<PlayerController>();
                player.player = i;
            }
            else {
                fish = Instantiate(npcPrefab) as FishController;
            }

            if (i == 0 || i < AppManager.instance.playerCount) {
                CameraController cam = Instantiate(playerCameraPrefab) as CameraController;
                cam.target = fish.transform;
                Rect r = cam.camera.rect;
                if (AppManager.instance.playerCount > 1) {
                    r.width = 0.5f;
                    r.x = (i % 2) * 0.5f;
                }
                if (AppManager.instance.playerCount > 2) {
                    r.height = 0.5f;
                    r.y = 0.5f - (i / 2) * 0.5f;
                }
                cam.camera.rect = r;

                PlayerUIController playerUI = cam.GetComponent<PlayerUIController>();
                playerUI.player = i;
            }

            fish.player = i;

            fish.GetComponent<SpriteRenderer>().color = AppManager.instance.playerColors[i];

            fish.transform.position = new Vector3(
                track.startPoint.x + i * 0.75f,
                track.startPoint.y + (i % 2) * 1.25f,
                0
            ); 

            ranking.Add(fish);
        }

        checkpointCount = track.checkpoints.Length;

        Cursor.visible = false;
        StartCoroutine("CountdownCoroutine");
    }

    void OnEnable () {
        FishController.onFinish += OnFishFinished;
        FishController.onCheckpointReached += OnFishReachedCheckpoint;
    }

    void OnDisable () {
        FishController.onFinish -= OnFishFinished;
        FishController.onCheckpointReached -= OnFishReachedCheckpoint;
    }

    bool gameOver;
    void OnFishFinished (FishController fish) {
        if (!results.Contains(fish)) {
            onResult(fish, results.Count);
            results.Add(fish);
        }
        if (!gameOver) {
            int playersDone = 0;
            int racers = AppManager.instance.racerCount;
            int players = AppManager.instance.playerCount;
            foreach (FishController fin in results) {
                if (fin.player < players) playersDone++;
            }
            if (playersDone == players) StartCoroutine("GameOver");
            else if (playersDone == players-1 && results.Count == racers-1) {
                foreach (FishController player in ranking) {
                    if (!results.Contains(player)) {
                        onResult(player, racers-1);
                        StartCoroutine("GameOver");
                    }
                }
            }
        }
    }

    void OnFishReachedCheckpoint (FishController fish) {
        ranking.Sort((f1, f2) => {
            if (f1.player == f2.player) return 0;
            else if (f1.lap != f2.lap) return f2.lap.CompareTo(f1.lap);
            else if (f1.checkpoint != f2.checkpoint) return f2.checkpoint.CompareTo(f1.checkpoint);
            else return f1.timeAtCheckpoint.CompareTo(f2.timeAtCheckpoint);
        });
    }

    public static int GetRanking (int player) {
        for (int i = 0; i < instance.ranking.Count; i++) {
            if (instance.ranking[i].player == player) return i;
        }
        return -1;
    }

    public static int GetProgress (int id) {
        for (int i = 0; i < instance.track.checkpoints.Length; i++) {
            if (instance.track.checkpoints[i].GetInstanceID() == id) return i;
        }
        return -1;
    }

    IEnumerator CountdownCoroutine () {
        bigText.text = "";
        yield return new WaitForSeconds(1);
        
        for (int i = 3; i > 0; i--) {
            bigText.text = i.ToString();
            AudioManager.PlayEffect("count");
            yield return new WaitForSeconds(1);
        }
        startTime = Time.time;
        bigText.text = "GO!";
        AudioManager.PlayEffect("count", null, 1, 2);
        canRace = true;
        for (float t = 1; t >= 0; t -= Time.deltaTime) {
            bigText.color = new Color(1,1,1,t);
            yield return new WaitForEndOfFrame();
        }
        bigText.color = Color.clear;
    }

    IEnumerator GameOver () {
        gameOver = true;
        yield return new WaitForSeconds(3);
        canRace = false;
        gameOver = false;
        SceneManager.LoadScene("Menu", LoadSceneMode.Single);
    }
}
}