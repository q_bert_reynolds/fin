<?xml version="1.0" encoding="utf-8"?>
<tileset firstgid="1" name="rockTiles" tilewidth="32" tileheight="32" spacing="2" margin="1" tilecount="36" columns="6">
 <image source="rockTiles.png" width="204" height="204" />
 <terraintypes>
  <terrain name="rock" tile="10" />
  <terrain name="darkRock" tile="28" />
 </terraintypes>
 <tile id="0" terrain="0,0,0,">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 0,32 24,32 32,24 32,0" />
   </object>
  </objectgroup>
 </tile>
 <tile id="1" terrain="0,0,,">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="32" height="24" />
  </objectgroup>
 </tile>
 <tile id="2" terrain="0,0,,0">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 0,24 8,32 32,32 32,0" />
   </object>
  </objectgroup>
 </tile>
 <tile id="3" terrain=",,,0">
  <objectgroup draworder="index">
   <object id="1" x="8" y="32">
    <polygon points="0,0 24,-24 24,0" />
   </object>
  </objectgroup>
 </tile>
 <tile id="4" terrain=",,0,0">
  <objectgroup draworder="index">
   <object id="1" x="0" y="8" width="32" height="24" />
  </objectgroup>
 </tile>
 <tile id="5" terrain=",,0,">
  <objectgroup draworder="index">
   <object id="1" x="0" y="8">
    <polygon points="0,0 24,24 0,24" />
   </object>
  </objectgroup>
 </tile>
 <tile id="6" terrain="0,,0,">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="24" height="32" />
  </objectgroup>
 </tile>
 <tile id="8" terrain=",0,,0">
  <objectgroup draworder="index">
   <object id="1" x="8" y="0" width="24" height="32" />
  </objectgroup>
 </tile>
 <tile id="9" terrain=",0,,0">
  <objectgroup draworder="index">
   <object id="1" x="8" y="0" width="24" height="32" />
  </objectgroup>
 </tile>
 <tile id="10" terrain="0,0,0,0">
  <objectgroup draworder="index">
   <object id="2" x="0" y="0" width="32" height="32" />
  </objectgroup>
 </tile>
 <tile id="11" terrain="0,,0,">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="24" height="32" />
  </objectgroup>
 </tile>
 <tile id="12" terrain="0,,0,0">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 24,0 32,8 32,32 0,32" />
   </object>
  </objectgroup>
 </tile>
 <tile id="13" terrain=",,0,0">
  <objectgroup draworder="index">
   <object id="1" x="0" y="8" width="32" height="24" />
  </objectgroup>
 </tile>
 <tile id="14" terrain=",0,0,0">
  <objectgroup draworder="index">
   <object id="1" x="0" y="32">
    <polygon points="0,0 0,-24 8,-32 32,-32 32,0" />
   </object>
  </objectgroup>
 </tile>
 <tile id="15" terrain=",0,,">
  <objectgroup draworder="index">
   <object id="1" x="8" y="0">
    <polygon points="0,0 24,24 24,0" />
   </object>
  </objectgroup>
 </tile>
 <tile id="16" terrain="0,0,,">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="32" height="24" />
  </objectgroup>
 </tile>
 <tile id="17" terrain="0,,,">
  <objectgroup draworder="index">
   <object id="1" x="0" y="24">
    <polygon points="0,0 24,-24 0,-24" />
   </object>
  </objectgroup>
 </tile>
 <tile id="18" terrain="1,1,1," />
 <tile id="19" terrain="1,1,," />
 <tile id="20" terrain="1,1,,1" />
 <tile id="21" terrain=",,,1" />
 <tile id="22" terrain=",,1,1" />
 <tile id="23" terrain=",,1," />
 <tile id="24" terrain="1,,1," />
 <tile id="26" terrain=",1,,1" />
 <tile id="27" terrain=",1,,1" />
 <tile id="28" terrain="1,1,1,1" />
 <tile id="29" terrain="1,,1," />
 <tile id="30" terrain="1,,1,1" />
 <tile id="31" terrain=",,1,1" />
 <tile id="32" terrain=",1,1,1" />
 <tile id="33" terrain=",1,," />
 <tile id="34" terrain="1,1,," />
 <tile id="35" terrain="1,,," />
</tileset>